#pragma once
#include <array>
#include <cstddef>
#include <memory>

template <typename T, size_t SMALL_SIZE>
struct socow_vector {
  using iterator = T*;
  using const_iterator = T const*;

  socow_vector() : size_(0), capacity_(SMALL_SIZE) {}

  socow_vector(socow_vector const& other)
      : size_(other.size_), capacity_(other.size_) {
    if (other.size_ > SMALL_SIZE) {
      dynamic_storage = other.dynamic_storage;
      dynamic_storage->ref_count++;
    } else {
      capacity_ = SMALL_SIZE;
      copy_raw(other.data(), other.size(), static_storage.data());
    }
  }

  socow_vector& operator=(socow_vector const& other) {
    if (this == &other) {
      return *this;
    }
    socow_vector(other).swap(*this);
    return *this;
  }

  ~socow_vector() {
    reset_data();
  }

  T& operator[](size_t i) {
    return data()[i];
  }

  T const& operator[](size_t i) const {
    return data()[i];
  }

  T* data() {
    ensure_uniqueness();
    return data_without_copying();
  }

  T const* data() const {
    return const_data();
  }

  size_t size() const {
    return size_;
  }

  T& front() {
    return data()[0];
  }

  T const& front() const {
    return data()[0];
  }

  T& back() {
    return data()[size_ - 1];
  }

  T const& back() const {
    return data()[size_ - 1];
  }

  void push_back(T const& el) {
    if (size_ < capacity_) {
      new (data() + size_) T(el);
    } else if (size_ == capacity_) {
      size_t new_capacity = 2 * size_;
      flexible_array* tmp =
          copy_raw_flexible_array(new_capacity, const_data(), size_);
      try {
        new (tmp->buffer + size_) T(el);
      } catch (...) {
        reset_flexible_array(size_, tmp);
        throw;
      }
      reset_data();
      capacity_ = new_capacity;
      dynamic_storage = tmp;
    }
    size_++;
  }

  void pop_back() {
    (data() + size_ - 1)->~T();
    size_--;
  }

  bool empty() const {
    return size_ == 0;
  }

  size_t capacity() const {
    return capacity_;
  }

  void reserve(size_t new_capacity) {
    if (new_capacity <= capacity_)
      return;
    strong_reserve(new_capacity);
  }

  void shrink_to_fit() {
    if (size_ == capacity_) {
      return;
    }
    strong_reserve(size_);
  }

  void clear() {
    delete_elements(data(), size_);
    size_ = 0;
  }

  void swap(socow_vector& v) {
    if (is_small_object() != v.is_small_object()) {
      socow_vector* dynamic_v = this;
      socow_vector* static_v = &v;
      if (is_small_object()) {
        std::swap(dynamic_v, static_v);
      }
      flexible_array* temp = dynamic_v->dynamic_storage;
      try {
        copy_raw(static_v->static_storage.data(), static_v->size(),
                 dynamic_v->static_storage.data());
      } catch (...) {
        dynamic_v->dynamic_storage = temp;
        throw;
      }
      static_v->reset_data();
      static_v->dynamic_storage = temp;
    } else if (is_small_object()) {
      static_array* big_arr = &v.static_storage;
      static_array* small_arr = &static_storage;
      size_t size1 = v.size();
      size_t size2 = size_;
      if (size1 < size2) {
        std::swap(big_arr, small_arr);
        std::swap(size1, size2);
      }
      copy_raw(big_arr->data() + size2, size1 - size2, small_arr->data() + size2);
      for (size_t i = 0; i < size2; i++) {
        std::swap(v.static_storage[i], static_storage[i]);
      }
      for (size_t i = size2; i < size1; i++) {
        (big_arr->data() + i)->~T();
      }
    } else {
      std::swap(dynamic_storage, v.dynamic_storage);
    }
    std::swap(v.size_, size_);
    std::swap(v.capacity_, capacity_);
  }

  iterator begin() {
    return data();
  }

  iterator end() {
    return data() + size_;
  }

  const_iterator begin() const {
    return data();
  }

  const_iterator end() const {
    return data() + size_;
  }

  iterator insert(const_iterator pos, T const& el) {
    ptrdiff_t index = pos - const_data();
    push_back(el);
    T* pointer = data();
    for (size_t i = size_ - 1; i > index; i--) {
      std::swap(pointer[i], pointer[i - 1]);
    }
    return pointer;
  }

  iterator erase(const_iterator pos) {
    return erase(pos, pos + 1);
  }

  iterator erase(const_iterator first, const_iterator last) {
    ptrdiff_t cnt_deleted = last - first;
    ptrdiff_t ind_start = first - const_data();
    ptrdiff_t ind_end = last - const_data();
    for (iterator it = data() + ind_start + cnt_deleted;
         it != data_without_copying() + size_; it++) {
      std::swap(*it, *(it - cnt_deleted));
    }
    for (size_t i = 0; i < cnt_deleted; i++) {
      pop_back();
    }
    return data_without_copying() + ind_end - cnt_deleted;
  }

private:
  using static_array = std::array<T, SMALL_SIZE>;
  struct flexible_array {
    size_t ref_count = 1;
    T buffer[0];

    flexible_array() = default;
    ~flexible_array() {}
  };

  size_t size_;
  size_t capacity_;
  union {
    static_array static_storage;
    flexible_array* dynamic_storage = nullptr;
  };

  static flexible_array* create_flexible_array(size_t n) {
    size_t bytes = sizeof(flexible_array) + n * sizeof(T);
    std::byte* buffer = static_cast<std::byte*>(operator new(bytes));
    flexible_array* p = new (buffer) flexible_array{};
    return p;
  }

  static void reset_flexible_array(size_t size, flexible_array* p) {
    delete_elements(p->buffer, size);
    operator delete(reinterpret_cast<std::byte*>(p));
  }

  static void delete_elements(T* buffer, size_t size) {
    for (size_t i = size; i > 0; i--) {
      buffer[i - 1].~T();
    }
  }

  bool is_small_object() const {
    return capacity_ == SMALL_SIZE;
  }

  bool unique() {
    return dynamic_storage->ref_count == 1;
  }

  void ensure_uniqueness() {
    if (!is_small_object() && !unique()) {
      flexible_array* temp = dynamic_storage;
      dynamic_storage =
          copy_raw_flexible_array(capacity_, dynamic_storage->buffer, size_);
      temp->ref_count--;
    }
  }

  void reset_data() {
    if (is_small_object()) {
      delete_elements(data(), size_);
    } else if (unique()) {
      reset_flexible_array(size_, dynamic_storage);
    } else {
      dynamic_storage->ref_count--;
    }
  }

  void strong_reserve(size_t new_capacity) {
    if (new_capacity > SMALL_SIZE) {
      flexible_array* tmp =
          copy_raw_flexible_array(new_capacity, const_data(), size_);
      reset_data();
      dynamic_storage = tmp;
      capacity_ = new_capacity;
    } else if (!is_small_object()) {
      flexible_array* temp = dynamic_storage;
      try {
        copy_raw(temp->buffer, size_, static_storage.data());
      } catch (...) {
        dynamic_storage = temp;
        throw;
      }
      if (temp->ref_count == 1) {
        reset_flexible_array(size_, temp);
      } else {
        (temp->ref_count)--;
      }
      capacity_ = SMALL_SIZE;
    }
  }

  static flexible_array* copy_raw_flexible_array(size_t capacity, const T* src,
                                                 size_t size) {
    flexible_array* dst = create_flexible_array(capacity);
    try {
      copy_raw(src, size, dst->buffer);
    } catch (...) {
      operator delete(reinterpret_cast<std::byte*>(dst));
      throw;
    }
    return dst;
  }

  static void copy_raw(const_iterator src, size_t size, iterator dst) {
    size_t i = 0;
    try {
      for (i; i < size; i++) {
        new (dst + i) T(src[i]);
      }
    } catch (...) {
      for (size_t j = i; j > 0; j--) {
        (dst + j - 1)->~T();
      }
      throw;
    }
  }

  T const* const_data() const {
    if (is_small_object()) {
      return static_storage.data();
    }
    return dynamic_storage->buffer;
  }

  T* data_without_copying() {
    if (is_small_object()) {
      return static_storage.data();
    }
    return dynamic_storage->buffer;
  }
};

